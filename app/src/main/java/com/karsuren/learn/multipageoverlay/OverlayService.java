package com.karsuren.learn.multipageoverlay;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.karsuren.learn.multipageoverlay.util.OverlayAnimations;
import com.karsuren.learn.multipageoverlay.util.L;

import java.util.Stack;

public abstract class OverlayService extends Service
        implements OverlayRootLayout.BackPressedListener, IOverlayService {

    private WindowManager mWindowManager;
    private LayoutInflater mLayoutInflater;
    private WindowManager.LayoutParams mLayoutParams;

    private OverlayRootLayout mRootView;
    private FrameLayout mPageContainer;

    private boolean mOverlayVisible;

    private Stack<OverlayPage> mPageStack;

    public OverlayService() {
        L.d("entering OverlayService constructor");
    }

    @Override
    public IBinder onBind(Intent intent) {
        L.d("entering OverlayService.onBind()");
        return null;
    }

    @Override
    public void onCreate() {
        L.d("entering OverlayService.onCreate()");
        super.onCreate();

        mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        mLayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mLayoutParams = getLayoutParams();

        mRootView = (OverlayRootLayout) mLayoutInflater.inflate(R.layout.overlay_root_layout, null);
        mRootView.setBackPressedListener(this);
        mOverlayVisible = false;

        mPageContainer = mRootView.findViewById(R.id.page_container);
        mPageStack = new Stack<>();

        OverlayPage landingPage = getLandingPage();
        try {
            landingPage.inflateView(mLayoutInflater, this);
        } catch (OverlayPage.PageAlreadyInflatedException e) {
            // Cancel the service if landing page provided is already infalted
            e.printStackTrace();
            L.d("Cannot create overlay -- landing page already inflated");
            stopSelf();
            return;
        }
        landingPage.onShowPage();
        mPageContainer.addView(landingPage.getView());
        mPageStack.push(landingPage);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        L.d("entering OverlayService.onStartCommand()");
        showOverlay();

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        L.d("entering OverlayService.onDestroy()");
        super.onDestroy();

        closeOverlay();
    }

    private WindowManager.LayoutParams getLayoutParams() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels/2;
        int height = metrics.heightPixels/2;

        int layoutType = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)?
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY:
                WindowManager.LayoutParams.TYPE_PHONE;

        int flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(width,
                height,
                layoutType,
                flags,
                PixelFormat.TRANSLUCENT);

        layoutParams.gravity = Gravity.CENTER;
        layoutParams.x = 0;
        layoutParams.y = 0;

        return layoutParams;
    }

    private void showOverlay() {
        if (!mOverlayVisible) {
            Animator animator = new OverlayAnimations().fadeAndSlideInAnimator(mRootView);
            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    super.onAnimationEnd(animation);
                    mWindowManager.addView(mRootView, mLayoutParams);
                    mOverlayVisible = true;
                }
            });
            animator.start();
        }
    }

    private void closeOverlay() {
        if (mOverlayVisible) {
            Animator animator = new OverlayAnimations().fadeAndSlideOutAnimator(mRootView);
            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mRootView.setVisibility(View.GONE);
                    mWindowManager.removeView(mRootView);
                    mOverlayVisible = false;
                }
            });
            animator.start();
        }
    }

    private void popPageStack() {
        if (mPageStack.size() > 1) {
            final OverlayPage currentTopPage = mPageStack.pop();
            final OverlayPage oldPage = mPageStack.peek();
            Animator animator = new OverlayAnimations(OverlayAnimations.QUICK).
                    removePageAnimator(currentTopPage.getView(), oldPage.getView());
            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    super.onAnimationStart(animation);
                    oldPage.getView().setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mPageContainer.removeView(currentTopPage.getView());
                }
            });
            currentTopPage.onHidePage();
            currentTopPage.onDestroyPage();
            oldPage.onShowPage();
            animator.start();
        } else if (mPageStack.size() == 1) {
            mPageStack.peek().onHidePage();
            mPageStack.peek().onDestroyPage();
            stopSelf();
        } else {
            L.wtf("On page stack pop - Overlay page stack empty");
        }
    }

    private void pushToPageStack(final OverlayPage newPage) {
        if (mPageStack.size() == 0) {
            L.wtf("On page stack push - Overlay page stack empty");
            return;
        }
        final OverlayPage currentTopPage = mPageStack.peek();
        Animator animator = new OverlayAnimations(OverlayAnimations.QUICK).
                addNewPageAnimator(currentTopPage.getView(), newPage.getView());
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                mPageContainer.addView(newPage.getView());
                mPageStack.push(newPage);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                currentTopPage.getView().setVisibility(View.GONE);
            }
        });
        currentTopPage.onHidePage();
        newPage.onShowPage();
        animator.start();
    }

    @Override
    public void onBackPressed() {
        popPageStack();
    }

    @Override
    public void addPage(OverlayPage page) {
        try {
            page.inflateView(mLayoutInflater, this);
        } catch (OverlayPage.PageAlreadyInflatedException e) {
            // Don't add page to page stack is page is already inflated
            e.printStackTrace();
            L.d("Cannot add page to overlay. page already inflated");
            return;
        }
        pushToPageStack(page);
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    public abstract OverlayPage getLandingPage();
}