package com.karsuren.learn.multipageoverlay.util;

import android.util.Log;

public class L {
    public static final String TAG = "surenkar";

    public static void d(String message) {
        Log.d(TAG, message);
    }

    public static void wtf(String message) {
        Log.wtf(TAG, message);
    }
}
