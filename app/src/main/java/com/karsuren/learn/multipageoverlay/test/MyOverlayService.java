package com.karsuren.learn.multipageoverlay.test;

import android.content.Intent;
import android.os.IBinder;

import com.karsuren.learn.multipageoverlay.OverlayPage;
import com.karsuren.learn.multipageoverlay.OverlayService;

public class MyOverlayService extends OverlayService {
    @Override
    public OverlayPage getLandingPage() {
        return new Page1();
    }
}
