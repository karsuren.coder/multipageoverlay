package com.karsuren.learn.multipageoverlay;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.karsuren.learn.multipageoverlay.test.MyOverlayService;
import com.karsuren.learn.multipageoverlay.util.L;

public class MainActivity extends AppCompatActivity {
    public MainActivity() {
        super();
        L.d("MainActivity constructor - after super()");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        L.d("entering MainActivity.onCreate()");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button startOverlayButton = findViewById(R.id.start_overlay_button);
        startOverlayButton.setOnClickListener(view -> {
            L.d("Inside button click listener");
            startService(new Intent(MainActivity.this.getApplicationContext(),
                    MyOverlayService.class));
        });
    }

    @Override
    protected void onStart() {
        L.d("entering MainActivity.onStart()");
        super.onStart();
    }

    @Override
    protected void onResume() {
        L.d("entering MainActivity.onResume()");
        super.onResume();
    }

    @Override
    protected void onPause() {
        L.d("entering MainActivity.onPause()");
        super.onPause();
    }

    @Override
    protected void onStop() {
        L.d("entering MainActivity.onStop()");
        super.onStop();
    }

    @Override
    protected void onRestart() {
        L.d("entering MainActivity.onRestart()");
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        L.d("entering MainActivity.onDestroy()");
        super.onDestroy();
    }
}