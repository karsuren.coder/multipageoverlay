package com.karsuren.learn.multipageoverlay;

import android.view.LayoutInflater;
import android.view.View;

public abstract class OverlayPage {
    public static class PageAlreadyInflatedException extends Exception{}

    private View mView;

    protected View getView() {
        return mView;
    }

    public void inflateView(LayoutInflater inflater, IOverlayService service) throws PageAlreadyInflatedException{
        if (null != mView) {
            throw new PageAlreadyInflatedException();
        }
        mView = createView(inflater, service);
    }

    public abstract View createView(LayoutInflater inflater, IOverlayService service);
    public abstract void onShowPage();
    public abstract void onHidePage();
    public abstract void onDestroyPage();
}
