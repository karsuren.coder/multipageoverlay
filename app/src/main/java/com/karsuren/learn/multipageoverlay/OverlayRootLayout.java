package com.karsuren.learn.multipageoverlay;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.karsuren.learn.multipageoverlay.util.L;

public class OverlayRootLayout extends LinearLayout {

    public interface BackPressedListener {
        void onBackPressed();
    }

    BackPressedListener mBackPressedListener;

    public OverlayRootLayout(Context context) {
        super(context);
        L.d("In OverlayRootLayout constructor1");
    }

    public OverlayRootLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        L.d("In OverlayRootLayout constructor2");
    }

    public OverlayRootLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        L.d("In OverlayRootLayout constructor3");
    }

    public OverlayRootLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        L.d("In OverlayRootLayout constructor4");
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        L.d("entering OverlayRootLayout.dispatchKeyEvent()");

        if (null != event) {
            int action = event.getAction();
            int keycode = event.getKeyCode();
            if (KeyEvent.KEYCODE_BACK == keycode && KeyEvent.ACTION_UP == action) {
                L.d("OverlayRootLayout.dispatchKeyEvent() -- back button released");
                if (null != mBackPressedListener) {
                    L.d("OverlayRootLayout.dispatchKeyEvent() -- dispatching back press listener");
                    mBackPressedListener.onBackPressed();
                    return true;
                } else {
                    L.d("OverlayRootLayout.dispatchKeyEvent() -- no back press listener found");
                }
            }
        }

        return super.dispatchKeyEvent(event);
    }

    public void setBackPressedListener(BackPressedListener listener) {
        mBackPressedListener = listener;
    }
}
