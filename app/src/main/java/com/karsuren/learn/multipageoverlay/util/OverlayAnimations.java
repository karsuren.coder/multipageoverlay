package com.karsuren.learn.multipageoverlay.util;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;
import android.view.animation.LinearInterpolator;

public class OverlayAnimations {

    private long mDuration;

    public static final long QUICK = 50;
    public static final long SLOW = 200;

    public OverlayAnimations() {
        mDuration = SLOW;
    }

    public OverlayAnimations(long duration) {
        mDuration = duration;
    }

    public Animator slideOutAnimator(View target) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target,
                "translationX",
                0f, 100f);
        animator.setDuration(mDuration);
        animator.setInterpolator(new LinearInterpolator());
        return animator;
    }

    public Animator slideInAnimator(View target) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target,
                "translationX",
                100f, 0f);
        animator.setDuration(mDuration);
        animator.setInterpolator(new LinearInterpolator());
        return animator;
    }

    public Animator fadeOutAnimator(View target) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target,
                "alpha",
                1f, 0f);
        animator.setDuration(mDuration);
        animator.setInterpolator(new LinearInterpolator());
        return animator;
    }

    public Animator fadeInAnimator(View target) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target,
                "alpha",
                0f, 1f);
        animator.setDuration(mDuration);
        animator.setInterpolator(new LinearInterpolator());
        return animator;
    }

    public Animator fadeAndSlideOutAnimator(View target) {
        Animator fadeAnimator = fadeOutAnimator(target);
        Animator slideAnimator = slideOutAnimator(target);
        AnimatorSet animator = new AnimatorSet();
        animator.playTogether(fadeAnimator, slideAnimator);
        return animator;
    }

    public Animator fadeAndSlideInAnimator(View target) {
        Animator fadeAnimator = fadeInAnimator(target);
        Animator slideAnimator = slideInAnimator(target);
        AnimatorSet animator = new AnimatorSet();
        animator.playTogether(fadeAnimator, slideAnimator);
        return animator;
    }

    public Animator addNewPageAnimator(View currentPage, View newPage) {
        Animator fadeOutAnimator = fadeOutAnimator(currentPage);
        Animator slideInAnimator = fadeAndSlideInAnimator(newPage);
        AnimatorSet animator = new AnimatorSet();
        animator.playTogether(fadeOutAnimator, slideInAnimator);
        return animator;
    }

    public Animator removePageAnimator(View currentPage, View oldPage) {
        Animator slideOutAnimator = fadeAndSlideOutAnimator(currentPage);
        Animator fadeInAnimator = fadeInAnimator(oldPage);
        AnimatorSet animator = new AnimatorSet();
        animator.playTogether(slideOutAnimator, fadeInAnimator);
        return animator;
    }
}
