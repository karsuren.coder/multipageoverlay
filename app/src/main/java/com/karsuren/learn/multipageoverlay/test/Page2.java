package com.karsuren.learn.multipageoverlay.test;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.karsuren.learn.multipageoverlay.IOverlayService;
import com.karsuren.learn.multipageoverlay.OverlayPage;
import com.karsuren.learn.multipageoverlay.R;
import com.karsuren.learn.multipageoverlay.util.L;

public class Page2 extends OverlayPage {
    IOverlayService mOverlayService;
    View mRootView;

    @Override
    public View createView(LayoutInflater inflater, IOverlayService service) {
        L.d("Page 2 onCreateView");
        mOverlayService = service;
        mRootView = inflater.inflate(R.layout.second_page, null);

        final EditText textField = mRootView.findViewById(R.id.name_field);

        Button helloButton = mRootView.findViewById(R.id.hello_bt);
        helloButton.setOnClickListener(view -> {
            String name = textField.getText().toString();
            Toast.makeText(mOverlayService.getAppContext(), 
                    null == name ? "" : "Hello Mr."+name, Toast.LENGTH_SHORT).
                    show();
        });
        return mRootView;
    }

    @Override
    public void onShowPage() {
        L.d("Page 2 onShow");
    }

    @Override
    public void onHidePage() {
        L.d("Page 2 onHide");
    }

    @Override
    public void onDestroyPage() {
        L.d("Page 2 onDestroy");
    }
}
