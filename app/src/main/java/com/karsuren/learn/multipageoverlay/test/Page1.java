package com.karsuren.learn.multipageoverlay.test;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.karsuren.learn.multipageoverlay.IOverlayService;
import com.karsuren.learn.multipageoverlay.OverlayPage;
import com.karsuren.learn.multipageoverlay.R;
import com.karsuren.learn.multipageoverlay.util.L;

public class Page1 extends OverlayPage {
    IOverlayService mOverlayService;
    View mRootView;

    @Override
    public View createView(LayoutInflater inflater, IOverlayService service) {
        L.d("Page 1 onCreateView");
        mOverlayService = service;
        mRootView = inflater.inflate(R.layout.first_page, null);
        Button button = mRootView.findViewById(R.id.trigger_page2_bt);
        button.setOnClickListener(view -> {
            mOverlayService.addPage(new Page2());
        });
        return mRootView;
    }

    @Override
    public void onShowPage() {
        L.d("Page 1 onShow");
    }

    @Override
    public void onHidePage() {
        L.d("Page 1 onHide");
    }

    @Override
    public void onDestroyPage() {
        L.d("Page 1 onDestroy");
    }
}
