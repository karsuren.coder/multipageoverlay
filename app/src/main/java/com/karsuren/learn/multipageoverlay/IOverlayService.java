package com.karsuren.learn.multipageoverlay;

import android.content.Context;

public interface IOverlayService {
    void addPage(OverlayPage page);
    Context getAppContext();
}
